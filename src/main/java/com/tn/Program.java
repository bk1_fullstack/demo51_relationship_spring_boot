package com.tn;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

import java.util.List;

public class Program {
    public static void main(String[] args) {
        Session session = null;
        try {
            session = buildSessionFactory().openSession();
            session.beginTransaction();
            Query<Account> query =session.createQuery("from Account ");

            List<Account> accounts = query.list();
            accounts.forEach(obj ->{
                System.out.println(obj.getId());
                System.out.println(obj.getUsername());
                System.out.println(obj.getPASSWORD());
                System.out.println(obj.getFull_name());

                List<Address> addresses=obj.getAddresses();
                System.out.println("size" + addresses.size());

                addresses.forEach(obj2->{
                    System.out.println(obj2.getId());
                    System.out.println(obj2.getStreet());
                });

            });

        }
        finally {
            if (session != null) {
                session.close();
            }
        }
    }

    private static SessionFactory buildSessionFactory() {
        // load configuration
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");

        // add entity
        configuration.addAnnotatedClass(Account.class);
        configuration.addAnnotatedClass(Address.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();

        return configuration.buildSessionFactory(serviceRegistry);
    }
}

